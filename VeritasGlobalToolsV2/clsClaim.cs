﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsClaim
    {
        private string sCon;
        private long lClaimID;
        private string SQL;
        private string lUserID;
        private string sDatePaid;

        public string CON
        {
            get
            {
                CON = sCon;
                return CON;
            }
            set
            {
                sCon = value;
            }
        }
        public long ClaimID
        {
            get
            {
                ClaimID = lClaimID;
                return ClaimID;
            }
            set
            {
                lClaimID = value;
            }
        }
        public string DatePaid
        {
            get
            {
                DatePaid = sDatePaid;
                return DatePaid;
            }
            set
            {
                sDatePaid = value;
            }
        }
        public long UserID
        {
            get
            {
                UserID = long.Parse(lUserID);
                return UserID;
            }
            set
            {
                lUserID = value.ToString();
            }
        }

        public void ProcessClaimStatus()
        {
            clsDBO.clsDBO dBO = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = " + lClaimID + " " +
                  "and claimdetailstatus <> 'Paid' " +
                  "and claimdetailstatus <> 'Cancelled' " +
                  "and claimdetailstatus <> 'Denied' ";
            dBO.OpenDB(SQL, sCon);
            if (dBO.RowCount() > 0)
            {
                return;
            }

            SQL = "select * from claimdetail " +
                  "where claimid = " + lClaimID + " " +
                  "and jobno like 'j%' " +
                  "and claimdetailstatus = 'Paid' ";
            dBO.OpenDB(SQL, sCon);
            if (dBO.RowCount() > 0)
            {
                SQL = "update claim " +
                      "set status = 'Paid', " +
                      "moddate = '" + DateTime.Today + "', " +
                      "modby = " + lUserID + ", " +
                      "datepaid = '" + sDatePaid + "', " +
                      "paidby = " + lUserID + " " +
                      "where claimid = " + lClaimID + " " +
                      "and status = 'Open' ";
                dBO.RunSQL(SQL, sCon);
            }

            SQL = "select * from claimdetail " + 
                  "where claimid = " + lClaimID + " " + 
                  "and jobno = 'A01' ";
            dBO.OpenDB(SQL, sCon);
            if (dBO.RowCount() > 0)
            {
                dBO.GetRow();
                if (dBO.GetFields("claimdetailstatus").ToString().ToLower().Trim() == "denied")
                {
                    SQL = "update claim " +
                          "set status = 'Denied', " +
                          "moddate = '" + DateTime.Today + "', " +
                          "modby = " + lUserID + " " +
                          "where claimid = " + lClaimID + " " +
                          "and status = 'Open' ";
                    dBO.RunSQL(SQL, sCon);
                }
                else if (dBO.GetFields("claimdetailstatus").ToString().ToLower().Trim() == "cancelled")
                {
                    SQL = "update claim " +
                          "set status = 'Void', " +
                          "moddate = '" + DateTime.Today + "', " +
                          "modby = " + lUserID + " " +
                          "where claimid = " + lClaimID + " " +
                          "and status = 'Open' ";
                    dBO.RunSQL(SQL, sCon);
                }
                else
                {
                    SQL = "update claim " +
                          "set status = 'Paid', " +
                          "moddate = '" + DateTime.Today + "', " +
                          "modby = " + lUserID + ", " +
                          "datepaid = '" + sDatePaid + "', " +
                          "paidby = " + lUserID + " " +
                          "where claimid = " + lClaimID + " " +
                          "and status = 'Open'";
                    dBO.RunSQL(SQL, sCon);
                }
            }
        }
    }
}
