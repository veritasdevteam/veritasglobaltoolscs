﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsMoxyAddressChange
    {
        private string SQL;
        private long lContractID;
        private clsDBO.clsDBO clC = new clsDBO.clsDBO();
        private string sCustomerID;

        public void UpdateMoxy(long xContractID)
        {
            lContractID = xContractID;
            OpenContract();
        }
        private void OpenContract()
        {
            SQL = "select * from contract " +
                  "where contractid = " + lContractID;
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sCustomerID = "";
                GetCustomerID();
                AddAddressChange();
            }
        }
        private void GetCustomerID()
        {
            clsDBO.clsDBO clM = new clsDBO.clsDBO();
            if (clC.GetFields("contractno").Substring(0, 3) == "VEP" || clC.GetFields("contractno").Substring(0, 3) == "REP") 
            {
                SQL = "select * from VeritasMoxy.dbo.epcontract ";
            }
            else
            {
                SQL = "select * from VeritasMoxy.dbo.moxycontract ";
            }
            SQL = SQL + "where contractno = '" + clC.GetFields("contractno") + "' ";
            clM.OpenDB(SQL, Global.sCON);
            if (clM.RowCount() > 0)
            {
                clM.GetRow();
                sCustomerID = clM.GetFields("CustomerID");
            }
        }
        private void AddAddressChange()
        {
            clsDBO.clsDBO clM = new clsDBO.clsDBO();

            if (clC.GetFields("contractno").Substring(0, 3) == "VEP" || clC.GetFields("contractno").Substring(0, 3) == "REP") 
            {
                SQL = "select * from VeritasMoxy.dbo.epaddresschange ";
                SQL = SQL + "where epaddresschangeid = 0 ";
            }
            else
            {
                SQL = "select * from VeritasMoxy.dbo.moxyaddresschange ";
                SQL = SQL + "where moxyaddresschangeid = 0 ";
            }
            clM.OpenDB(SQL, Global.sCON);
            if (clM.RowCount() == 0)
            {
                clM.NewRow();
                clM.SetFields("customerid", sCustomerID);
                clM.SetFields("fname", clC.GetFields("fname"));
                clM.SetFields("lname", clC.GetFields("lname"));
                clM.SetFields("addr1", clC.GetFields("addr1"));
                clM.SetFields("addr2", clC.GetFields("addr2"));
                clM.SetFields("city", clC.GetFields("city"));
                clM.SetFields("state", clC.GetFields("state"));
                clM.SetFields("zip", clC.GetFields("zip"));
                clM.SetFields("phone", clC.GetFields("phone"));
                clM.SetFields("vgcontractid", clC.GetFields("contractid"));
                clM.SetFields("changedate", DateTime.Now.ToString());
                clM.AddRow();
                clM.SaveDB();
            }
        }
    }
}
